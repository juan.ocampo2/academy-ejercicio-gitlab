1- Crear un repositorio público en Gitlab a partir del proyecto dado.
2- Las features a desarrollar consisten escribir algoritmos dentro de las funciones de los 4 modifiers que retornen el resultado esperado.
3- Tendremos dos tareas (task-1 y task-2) donde cada una incluirá 2 features repartidas a elección
4- Cada tarea tendrá su propia branch que parte de Integration, y se desarrollarán sus features en branchs independientes. Ej:
Rama de tarea: FB-Task-1
Ramas de desarrollo que parten de FB-Task-1: array-modifier y string-modifier
5- Al terminar las features deben confluir en la branch de tarea correspondientes
6- Las branch de las tareas deben hacer el MR a Integration
7- Pasar las MR al instructor para que haga code review de las mismas