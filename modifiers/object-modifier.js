function objectModifier(inputObject) {
/* ***
 * 
 * Return expected:
 * 
 * {
 *  eman: "name: John",
 *  emaNtsal: "lastName: Doe",
 *  enohp: 5678,
 *  delbanEsi: false
 * }
 * 
*** */
}

module.exports = {
  objectModifier,
};
